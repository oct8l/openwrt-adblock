#!/bin/sh
#
# Originally from glitch on the gl-inet forum
#    https://forum.gl-inet.com/t/openvpn-periodic-disconnection/2296/28
#
# Adapted by oct8l (oct8l@oct8l.email)
#
# License: https://www.apache.org/licenses/LICENSE-2.0

if ! ping -I tun0 -c5 -w5 8.8.8.8; then
killall openvpn # 2>/dev/null (took this out because I don't understand it)
ovpn=$(uci get glconfig.openvpn.ovpn)
/usr/sbin/openvpn "$ovpn" &
(sleep 1; /etc/init.d/network reload)
echo ":::::vpn-recon-1.sh used:::::"
fi